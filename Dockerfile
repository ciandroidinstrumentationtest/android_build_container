FROM openjdk:8-jdk

RUN wget -O /home/cmdline-tools.zip  https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip && \
    apt-get install unzip && \
    mkdir -p home/android_tools/cmdline-tools/ && \
    unzip -q /home/cmdline-tools.zip -d home/android_tools/cmdline-tools/ && \
    rm -rf /home/cmdline-tools.zip  && \
    mv home/android_tools/cmdline-tools/cmdline-tools home/android_tools/cmdline-tools/tools && \
    mkdir -p home/android_tools/cmdline-tools/tools/latest/bin && \
    cp -r home/android_tools/cmdline-tools/tools/bin home/android_tools/cmdline-tools/tools/latest/bin && \
    wget -O /home/android_tools/platform_tools.zip https://dl.google.com/android/repository/platform-tools-latest-linux.zip && \
    unzip -q /home/android_tools/platform_tools.zip -d home/android_tools/ && \
    rm -rf /home/android_tools/platform_tools.zip && \
    wget -O /home/android_tools/gradle-6.6.1.zip https://services.gradle.org/distributions/gradle-6.6.1-all.zip && \
    unzip -q /home/android_tools/gradle-6.6.1.zip -d home/android_tools/ && \
    rm -rf /home/android_tools/gradle-6.6.1.zip && \
    export PATH="$PATH:/home/android_tools/platform-tools:$PATH" && \
    export ANDROID_SDK_ROOT="/home/android_tools" && \
    export PATH="$PATH:/home/android_tools/gradle-6.6.1/bin" && \
    export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/tools/bin" && \
    yes | sdkmanager --licenses && \
    sdkmanager --install "patcher;v4" && \
    sdkmanager --install "build-tools;30.0.3" && \
    sdkmanager --install "platform-tools"&& \
    sdkmanager --install "platforms;android-30"

ENV PATH "$PATH:/home/android_tools/platform-tools:$PATH"
ENV ANDROID_SDK_ROOT "/home/android_tools"
ENV PATH "$PATH:/home/android_tools/gradle-6.6.1/bin"
ENV PATH "$PATH:$ANDROID_SDK_ROOT/cmdline-tools/tools/bin"
